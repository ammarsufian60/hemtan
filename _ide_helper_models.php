<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Domains\Promotion\Models{
/**
 * App\Domains\Promotion\Models\Promotion
 *
 * @property int $id
 * @property string $name
 * @property string $name_arabic
 * @property string $description
 * @property string $description_arabic
 * @property string|null $mobile_number
 * @property string|null $open_hours
 * @property int $discount_rate
 * @property string $address
 * @property int|null $category_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domains\Promotion\Models\PromotionCategory|null $category
 * @property-read mixed $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereDescriptionArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereDiscountRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereMobileNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereNameArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereOpenHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $typeable_type
 * @property int|null $typeable_id
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $typeable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereTypeableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereTypeableType($value)
 * @property string|null $address_arabic
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\Promotion whereAddressArabic($value)
 */
	class Promotion extends \Eloquent {}
}

namespace App\Domains\Promotion\Models{
/**
 * App\Domains\Promotion\Models\PromotionCategory
 *
 * @property int $id
 * @property string $name
 * @property string $name_arabic
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Promotion\Models\Promotion[] $promotions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\PromotionCategory newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\PromotionCategory newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\PromotionCategory query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\PromotionCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\PromotionCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\PromotionCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\PromotionCategory whereNameArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Promotion\Models\PromotionCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class PromotionCategory extends \Eloquent {}
}

namespace App\Domains\Donation\Models{
/**
 * App\Domains\Donation\Models\DonationProvider
 *
 * @property int $id
 * @property array $name
 * @property array $description
 * @property array $address
 * @property string $mobile_number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $admin_name
 * @property-read null|string $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Donation\Models\DonationService[] $services
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider whereMobileNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationProvider whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class DonationProvider extends \Eloquent {}
}

namespace App\Domains\Donation\Models{
/**
 * App\Domains\Donation\Models\DonationService
 *
 * @property int $id
 * @property array $name
 * @property array $description
 * @property string $service_name
 * @property int $provider_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $admin_name
 * @property-read null|string $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Promotion\Models\Promotion[] $promotions
 * @property-read \App\Domains\Donation\Models\DonationProvider $provider
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService whereServiceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Donation\Models\DonationService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class DonationService extends \Eloquent {}
}

namespace App\Domains\Transaction\Models{
/**
 * App\Domains\Transaction\Models\Transaction
 *
 * @property int $id
 * @property int $user_id
 * @property string $reference_number
 * @property string $amount
 * @property string $target_wallet
 * @property string|null $status
 * @property string $description
 * @property \Illuminate\Support\Carbon $date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domains\User\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction filter(\App\Filters\QueryFilter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction ofUser($user)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereReferenceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereTargetWallet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $pos
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Transaction\Models\Transaction wherePos($value)
 */
	class Transaction extends \Eloquent {}
}

namespace App\Domains\Notification\Models{
/**
 * App\Domains\Notification\Models\Notification
 *
 * @property mixed $button_label_ar
 * @property mixed $button_label
 * @property mixed $description_ar
 * @property mixed $description
 * @property-read mixed $image
 * @property mixed $navigate_to
 * @property mixed $notification_message_ar
 * @property mixed $notification_message
 * @property mixed $title_ar
 * @property mixed $title
 * @property mixed $topic
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Notification\Models\Notification newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Notification\Models\Notification newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Notification\Models\Notification query()
 * @mixin \Eloquent
 * @property int $id
 * @property array $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Notification\Models\Notification whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Notification\Models\Notification whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Notification\Models\Notification whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Notification\Models\Notification whereUpdatedAt($value)
 */
	class Notification extends \Eloquent {}
}

namespace App\Domains\Location\Models{
/**
 * App\Domains\Location\Models\Location
 *
 * @property int $id
 * @property string $title
 * @property string|null $title_arabic
 * @property string $type
 * @property string|null $latitude
 * @property string|null $longitude
 * @property bool $cash_in
 * @property bool $cash_out
 * @property string|null $address
 * @property string|null $open_hours
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereCashIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereCashOut($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereOpenHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereTitleArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $city
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location filter(\App\Filters\QueryFilter $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereCity($value)
 * @property int|null $type_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereTypeId($value)
 * @property bool $registration
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\Location whereRegistration($value)
 */
	class Location extends \Eloquent {}
}

namespace App\Domains\Location\Models{
/**
 * App\Domains\Location\Models\LocationType
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Location\Models\Location[] $locations
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\LocationType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\LocationType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\LocationType query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\LocationType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\LocationType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\LocationType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Location\Models\LocationType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class LocationType extends \Eloquent {}
}

namespace App\Domains\User\Models{
/**
 * App\Domains\User\Models\User
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $nickname
 * @property string|null $wallet_number
 * @property string|null $user_token
 * @property string|null $balance
 * @property string|null $last_otp
 * @property string|null $last_request
 * @property int $preferred_language
 * @property string|null $email
 * @property string|null $email_verified_at
 * @property string|null $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read mixed $latest_transactions
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Transaction\Models\Transaction[] $transactions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User ofWallet($wallet_number)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereLastOtp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereLastRequest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereNickname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User wherePreferredLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereUserToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereWalletNumber($value)
 * @mixin \Eloquent
 * @property int $notifiable
 * @property string|null $fcm_token
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Wallet\Models\Recipient[] $recents
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereFcmToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereNotifiable($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Efawateercom\Models\FavoriteBill[] $favoriteBills
 * @property string $profile
 * @property int|null $status
 * @property int|null $is_basic
 * @property int|null $pending_approval
 * @property int|null $has_registered_basic
 * @property string|null $basic_activated_at
 * @property int|null $basic_status
 * @property string|null $balance_limit
 * @property string|null $consumed_balance
 * @property string|null $basic_age
 * @property mixed|null $rejection
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereBalanceLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereBasicActivatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereBasicAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereBasicStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereConsumedBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereHasRegisteredBasic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereIsBasic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User wherePendingApproval($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereProfile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereRejection($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\User\Models\User whereStatus($value)
 */
	class User extends \Eloquent {}
}

namespace App\Domains\Models{
/**
 * App\Domains\Models\ResponseLog
 *
 * @property int $id
 * @property int|null $user_id
 * @property string $url
 * @property mixed $headers
 * @property mixed $request
 * @property mixed $response
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domains\User\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog whereHeaders($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog whereRequest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog whereResponse($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Models\ResponseLog whereUserId($value)
 * @mixin \Eloquent
 */
	class ResponseLog extends \Eloquent {}
}

namespace App\Domains\Efawateercom\Models{
/**
 * App\Domains\Efawateercom\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property string $name_arabic
 * @property string $code
 * @property int $is_available
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Efawateercom\Models\Biller[] $billers
 * @property-read mixed $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category whereIsAvailable($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category whereNameArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Category available()
 */
	class Category extends \Eloquent {}
}

namespace App\Domains\Efawateercom\Models{
/**
 * App\Domains\Efawateercom\Models\ServiceOption
 *
 * @property int $id
 * @property string $name
 * @property string $name_arabic
 * @property string $identifier
 * @property int $service_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domains\Efawateercom\Models\BillerService $service
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption whereIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption whereNameArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\ServiceOption whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class ServiceOption extends \Eloquent {}
}

namespace App\Domains\Efawateercom\Models{
/**
 * App\Domains\Efawateercom\Models\Biller
 *
 * @property int $id
 * @property string $name
 * @property string $name_arabic
 * @property int|null $code
 * @property int $category_id
 * @property string $email
 * @property string $phone_number
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domains\Efawateercom\Models\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Efawateercom\Models\BillerService[] $services
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller whereNameArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\Biller ofCode($code)
 */
	class Biller extends \Eloquent {}
}

namespace App\Domains\Efawateercom\Models{
/**
 * App\Domains\Efawateercom\Models\BillerService
 *
 * @property int $id
 * @property int $biller_id
 * @property string $name
 * @property string $name_arabic
 * @property int $billing_no_required
 * @property string $type
 * @property mixed|null $prepaid_categories
 * @property string $service_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domains\Efawateercom\Models\Biller $biller
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Efawateercom\Models\ServiceOption[] $prepaidOptions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereBillerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereBillingNoRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereNameArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService wherePrepaidCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereServiceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\BillerService ofServiceType($type)
 */
	class BillerService extends \Eloquent {}
}

namespace App\Domains\Efawateercom\Models{
/**
 * App\Domains\Efawateercom\Models\FavoriteBill
 *
 * @property int $id
 * @property int $user_id
 * @property int $biller_id
 * @property string $billing_number
 * @property string $service_type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domains\Efawateercom\Models\Biller $biller
 * @property-read \App\Domains\User\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill whereBillerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill whereBillingNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill whereServiceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Efawateercom\Models\FavoriteBill whereName($value)
 */
	class FavoriteBill extends \Eloquent {}
}

namespace App\Domains\ZainVoucher\Models{
/**
 * App\Domains\ZainVoucher\Models\ZainVoucher
 *
 * @property int $id
 * @property string $type
 * @property string $tax_rate
 * @property string|null $amount
 * @property string|null $taxed_amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher whereTaxRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher whereTaxedAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\ZainVoucher\Models\ZainVoucher whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class ZainVoucher extends \Eloquent {}
}

namespace App\Domains\Wallet\Models{
/**
 * App\Domains\Wallet\Models\Recipient
 *
 * @property int $id
 * @property int $user_id
 * @property string $target_wallet
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Domains\User\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient ofUser($userId)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient whereTargetWallet($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Recipient whereName($value)
 */
	class Recipient extends \Eloquent {}
}

namespace App\Domains\Wallet\Models{
/**
 * App\Domains\Wallet\Models\Wallet
 *
 * @property-read \App\Domains\User\Models\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Wallet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Wallet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Wallet ofNumber($number)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Wallet ofUser($user_id)
 * @method static \Illuminate\Database\Query\Builder|\App\Domains\Wallet\Models\Wallet onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Wallet\Models\Wallet query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|\App\Domains\Wallet\Models\Wallet withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domains\Wallet\Models\Wallet withoutTrashed()
 * @mixin \Eloquent
 */
	class Wallet extends \Eloquent {}
}

namespace App\Domains\CreditCard\Models{
/**
 * App\Domains\CreditCard\Models\CreditCard
 *
 * @property int $id
 * @property string $program_id
 * @property string $text_color
 * @property string $lock_color
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read null|string $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard whereLockColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard whereProgramId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard whereTextColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\CreditCard\Models\CreditCard whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class CreditCard extends \Eloquent {}
}

namespace App\Domains\Banner\Models{
/**
 * App\Domains\Banner\Models\Banner
 *
 * @property int $id
 * @property string $title
 * @property array $shown_in
 * @property string $navigatable_type
 * @property int $navigatable_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $navigatable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner whereNavigatableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner whereNavigatableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner whereShownIn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Banner\Models\Banner whereUpdatedAt($value)
 * @mixin \Eloquent
 */
	class Banner extends \Eloquent {}
}

namespace App\Domains\Merchant\Models{
/**
 * App\Domains\Merchant\Models\Merchant
 *
 * @property-read null|string $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domains\Promotion\Models\Promotion[] $promotions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $name_arabic
 * @property string $description
 * @property string $description_arabic
 * @property string $city
 * @property string $address
 * @property string $address_arabic
 * @property string $service_name
 * @property string|null $mobile_number
 * @property string|null $type
 * @property string $longitude
 * @property string $latitude
 * @property string|null $open_hours
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereAddressArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereDescriptionArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereLatitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereLongitude($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereMobileNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereNameArabic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereOpenHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereServiceName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domains\Merchant\Models\Merchant whereCity($value)
 */
	class Merchant extends \Eloquent {}
}

