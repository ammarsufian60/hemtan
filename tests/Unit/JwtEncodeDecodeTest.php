<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JwtEncodeDecodeTest extends TestCase
{
    /**
     * Test encode_jwt and decode_jwt by example.
     *
     * @return void
     */
    public function testExample()
    {
        $data = "zain-cash-jwt-encode-decode-test";

        $token = encode_jwt($data);

        $this->assertEquals($data, decode_jwt($token));
    }
}
