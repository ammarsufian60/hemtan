<?php

return [
    'gateway' => [
        'url' => env('ZAIN_CASH_DOMAIN') . env('ZAIN_CASH_PREFIX'),
        'domain' => env('ZAIN_CASH_DOMAIN'),
        'prefix' => env('ZAIN_CASH_PREFIX'),
        'secret' => env('ZAIN_CASH_SECRET'),
    ],
    'wallet' => [
        'status' => [
            '-1' => [
                'en' => 'Wallet is not defined',
                'ar' => 'المحفظة غير معرفة'
            ],
            '1' => [
                'en' => 'Wallet is wrong',
                'ar' => 'خطأ في المعلومات'
            ],
            '2' => '',
            '3' => '',
            '4' => ''
        ]
    ],
    'basic_wallet' => [
        'images_directory' => env('ZAIN_CASH_BASIC_WALLET_IMAGES_DIRECTORY', 'Zain cash/'),
        'videos_directory' => env('ZAIN_CASH_BASIC_WALLET_VIDEOS_DIRECTORY', 'Zain cash/')
    ]
];
