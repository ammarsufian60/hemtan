<?php

return [
    'slack' => ['webhook' => env('SLACK_WEBHOOK')],
    'logging_enabled' => env('LOGGING_ENABLED',false),
];
