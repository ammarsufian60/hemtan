<?php
return [
    'map' => [
        'transfer_money',
        'cash_out',
        'merchant_payments',
        'cards_management',
        'zain_bills',
        'zain_recharge',
        'efwateercom_bills',
    ],
];
