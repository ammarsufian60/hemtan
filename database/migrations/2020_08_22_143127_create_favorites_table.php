<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('favorites', function (Blueprint $table) {
          $table->unsignedBigInteger('user_id');
          $table->foreign('user_id')->references('id')->on('users');

          $table->unsignedBigInteger('promotion_id');
          $table->foreign('promotion_id')->references('id')->on('promotions');

          $table->unique(['user_id','promotion_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('favorites');
    }
}
