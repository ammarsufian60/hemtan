<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAndPermissionsSeeder extends Seeder
{
    protected $roles;

    protected $actions;

    protected $resources;

    protected $features;

    public function __construct()
    {
        $this->roles = collect(['Super Admin', 'Merchant']);

        $this->actions = collect(['View', 'Create', 'Update', 'Delete']);

        $this->resources = collect([
            'Category', 'Branch', 'Merchant','Promotion', 'User','Brand'
        ]);

    }

    public function run()
    {
        $this->createRoles();
        $this->createPermissions();
        $this->attachPermissionsToRoles();

        $this->attachSuperAdminRoleToUser();
    }

    protected function createRoles()
    {
        $this->roles->each(function($role){
            Role::firstOrCreate([
                'name' => $role
            ]);
        });
    }

    protected function createPermissions()
    {
        $this->resources->each(function($resource) {
            $this->actions->each(function($action) use ($resource) {
                Permission::firstOrCreate(['name' => $this->getPermissionName($action, $resource)]);
            });
        });


    }

    protected function attachPermissionsToRoles()
    {
        Role::where('name', 'Super Admin')
            ->first()
            ->syncPermissions(Permission::pluck('name'));
    }

    protected function getPermissionName($action, $resource)
    {
        return "$action $resource";
    }

    protected function attachSuperAdminRoleToUser()
    {
        $users = \App\Domains\User\Models\User::whereIn('email', [
            'himtna.watan@gmail.com',
            'ammarsufian60@gmail.com',
        ])->get();

        $users->each(function ($user){
            if(!is_null($user) && !$user->hasRole('Super Admin')){
                $user->assignRole('Super Admin');
            }
        });
    }
}
