<p class="mt-8 text-center text-xs text-80">
    <span class="px-1">&middot;</span>
    &copy; {{ date('Y') }} Made with ♥ by  Ammar Daana
    <span class="px-1">&middot;</span>
</p>
