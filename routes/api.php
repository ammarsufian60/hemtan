<?php


/**
 * Authorization Requests
 */

Route::post('/login','User\Http\Controllers\AuthController@login');
Route::post('/register','User\Http\Controllers\AuthController@register');
Route::post('/forget-password','User\Http\Controllers\AuthController@forgetPassword');

/**
 * categories
 */

Route::get('/categories','Category\Http\Controller\CategoriesController@index');

/**
 * brands
 */
Route::middleware('auth:api')->get('/brands','Brand\Http\Controllers\BrandController@index');
Route::middleware('auth:api')->get('/discounted','Brand\Http\Controllers\DiscountedBrandsController');

/**
 * Promotions
 */
Route::get('/promotions','Promotion\Http\Controllers\PromotionController@index');
/**
 * Favorites
 */
Route::middleware('auth:api')->post('/favorite','Favorite\Http\Controllers\FavoriteController@store');
Route::middleware('auth:api')->get('/favorite','Favorite\Http\Controllers\FavoriteController@index');

/**
*Branchs API
*
*/

Route::middleware('auth:api')->get('/locations','Branch\Http\Controllers\BranchConroller@nearby');

Route::get('/qr','Promotion\Http\Controllers\GenerateQRController');

Route::get('/otp',function(\Illuminate\Http\Request $request)
{
    $oAuthAccessToken = 'e01b3e958fac567e3e0e30db39402fbd'; // OAuth 2.0 Access Token
    $client = new ReleansAPIClient($oAuthAccessToken);
    $message = $client->getMessage();
    $accept = 'Accept';
    $senderId = 'R7BErm0wMvbmwjEeYAlO1nqVy';
    $mobileNumber =$request->mobile_number; //$user->moblie_number;
    $body = 'Your Himtna Verification code is :  '.$request->otp;

    $result = $message->createSendSMSMessage($accept, $senderId, $mobileNumber, $body);
    return response()->json(['message' => 'success']);
});
