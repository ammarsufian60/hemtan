<?php

namespace App\Policies;

use App\Domains\User\Models\User;
use App\Domains\Promotion\Models\PromotionCategory;
use Illuminate\Auth\Access\HandlesAuthorization;

class PromotionCategoryPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('View Promotion Category');
    }

    /**
     * Determine whether the user can view the promotion category service.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\PromotionCategory  $promotionCategoryService
     * @return mixed
     */
    public function view(User $user, PromotionCategory $promotionCategoryService)
    {
        return $user->hasPermissionTo('View Promotion Category');
    }

    /**
     * Determine whether the user can create promotion category services.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('Create Promotion Category');
    }

    /**
     * Determine whether the user can update the promotion category service.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\PromotionCategory  $promotionCategoryService
     * @return mixed
     */
    public function update(User $user, PromotionCategory $promotionCategoryService)
    {
        return $user->hasPermissionTo('Update Promotion Category');
    }

    /**
     * Determine whether the user can delete the promotion category service.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\PromotionCategory  $promotionCategoryService
     * @return mixed
     */
    public function delete(User $user, PromotionCategory $promotionCategoryService)
    {
        return $user->hasPermissionTo('Delete Promotion Category');
    }

    /**
     * Determine whether the user can restore the promotion category service.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\PromotionCategory  $promotionCategoryService
     * @return mixed
     */
    public function restore(User $user, PromotionCategory $promotionCategoryService)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the promotion category service.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\PromotionCategory  $promotionCategoryService
     * @return mixed
     */
    public function forceDelete(User $user, PromotionCategory $promotionCategoryService)
    {
        return false;
    }
}
