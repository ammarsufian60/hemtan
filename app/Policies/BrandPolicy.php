<?php

namespace App\Policies;

use App\Domains\Branch\Models\Branch;
use App\Domains\Brand\Models\Brand;
use App\Domains\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BrandPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('View Brand');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  User
     * @param  Brand
     * @return mixed
     */
    public function view(User $user, Brand $model)
    {
        return $user->hasPermissionTo('View Brand');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  User
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('Super Admin');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  User
     * @param  Brand
     * @return mixed
     */
    public function update(User $user, Branch $model)
    {
        return $user->hasRole('Super Admin');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  Brand
     * @return mixed
     */
    public function delete(User $user, Brand $model)
    {
        return $user->hasRole('Super Admin');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\User\Models\User  $model
     * @return mixed
     */
    public function restore(User $user, Brand $model)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\User\Models\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, Brand $model)
    {
        return false;
    }
}
