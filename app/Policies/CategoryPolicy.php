<?php

namespace App\Policies;

use App\Domains\Category\Models\Category;
use App\Domains\User\Models\User;

use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('View Category');
    }

    /**
     * Determine whether the user can view the category.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param   $category
     * @return mixed
     */
    public function view(User $user, Category $category)
    {
        return $user->hasPermissionTo('View Category');
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('Create Category');
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param    $category
     * @return mixed
     */
    public function update(User $user, Category $category)
    {
        return $user->hasPermissionTo('Update Category');
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param   $category
     * @return mixed
     */
    public function delete(User $user, Category $category)
    {
        return $user->hasPermissionTo('Delete Category');
    }

    /**
     * Determine whether the user can restore the category.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param    $category
     * @return mixed
     */
    public function restore(User $user, Category $category)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the category.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param   $category
     * @return mixed
     */
    public function forceDelete(User $user, Category $category)
    {
        return false;
    }
}
