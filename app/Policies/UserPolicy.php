<?php

namespace App\Policies;

use App\Domains\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('View User');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\User\Models\User  $model
     * @return mixed
     */
    public function view(User $user, User $model)
    {
        return $user->hasPermissionTo('View User');
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasRole('Super Admin');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\User\Models\User  $model
     * @return mixed
     */
    public function update(User $user, User $model)
    {
        return $user->hasRole('Super Admin');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\User\Models\User  $model
     * @return mixed
     */
    public function delete(User $user, User $model)
    {
        return $user->hasRole('Super Admin');
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\User\Models\User  $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\User\Models\User  $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        return false;
    }
}
