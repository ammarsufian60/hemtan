<?php

namespace App\Policies;

use App\Domains\Models\ResponseLog;
use App\Domains\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Spatie\Permission\Models\Permission;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }


    /**
     * Determine whether the user can view the category.
     *
     * @param \App\Domains\User\Models\User $user
     * @param Permission $permission
     * @return mixed
     */
    public function view(User $user, Permission $permission)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can create categories.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can update the category.
     *
     * @param \App\Domains\User\Models\User $user
     * @param Permission $permission
     * @return mixed
     */
    public function update(User $user, Permission $permission)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can delete the category.
     *
     * @param \App\Domains\User\Models\User $user
     * @param Permission $permission
     * @return mixed
     */
    public function delete(User $user, Permission $permission)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    /**
     * Determine whether the user can restore the category.
     *
     * @param \App\Domains\User\Models\User $user
     * @param Permission $permission
     * @return mixed
     */
    public function restore(User $user, Permission $permission)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the category.
     *
     * @param \App\Domains\User\Models\User $user
     * @param Permission $permission
     * @return mixed
     */
    public function forceDelete(User $user, Permission $permission)
    {
        return false;
    }
}
