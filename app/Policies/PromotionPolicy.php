<?php

namespace App\Policies;

use App\Domains\Promotion\Models\Promotion;
use App\Domains\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PromotionPolicy
{
    use HandlesAuthorization;

    public function before($user, $ability)
    {
        if ($user->hasRole('Super Admin')) {
            return true;
        }
    }

    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('View Promotion');
    }

    /**
     * Determine whether the user can view the promotion.
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\Promotion $promotion
     * @return mixed
     */
    public function view(User $user, Promotion $promotion)
    {
        return $user->hasPermissionTo('View Promotion');
    }

    /**
     * Determine whether the user can create promotion .
     *
     * @param  \App\Domains\User\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('Create Promotion');
    }

    /**
     * Determine whether the user can update the promotion .
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\Promotion  $promotion
     * @return mixed
     */
    public function update(User $user, Promotion $promotion)
    {
        return $user->hasPermissionTo('Update Promotion');
    }

    /**
     * Determine whether the user can delete the promotion .
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\Promotion  $promotion
     * @return mixed
     */
    public function delete(User $user, Promotion $promotion)
    {
        return $user->hasPermissionTo('Delete Promotion');
    }

    /**
     * Determine whether the user can restore the promotion .
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\Promotion  $promotion
     * @return mixed
     */
    public function restore(User $user, Promotion $promotion)
    {
        return false;
    }

    /**
     * Determine whether the user can permanently delete the promotion .
     *
     * @param  \App\Domains\User\Models\User  $user
     * @param  \App\Domains\Promotion\Models\Promotion  $promotion
     * @return mixed
     */
    public function forceDelete(User $user, Promotion $promotion)
    {
        return false;
    }
}
