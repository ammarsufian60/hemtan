<?php

namespace App\Providers;

use App\Domains\Location\Models\Location;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Relations\Relation;

class MorphsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::morphMap([
            'category' => 'App\Domains\Efawateercom\Models\Category',
            'biller' => 'App\Domains\Efawateercom\Models\Biller',
            'biller_service' => 'App\Domains\Efawateercom\Models\BillerService',
            'promotion_category' => 'App\Domains\Promotion\Models\PromotionCategory',
            'location' => Location::class,
        ]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
