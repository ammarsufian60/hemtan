<?php

namespace App\Providers;

use App\Domains\Branch\Models\Branch;
use App\Domains\Brand\Models\Brand;
use App\Domains\Category\Models\Category;
use App\Domains\Donation\Models\DonationProvider;
use App\Domains\Donation\Models\DonationService;
use App\Domains\Location\Models\LocationType;
use App\Domains\Merchant\Models\Merchant;
use App\Domains\Models\ResponseLog;
use App\Domains\Notification\Models\Notification;
use App\Domains\Promotion\Models\Promotion;
use App\Domains\User\Models\User;
use App\Policies\BranchPolicy;
use App\Policies\BrandPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\DonationProviderPolicy;
use App\Policies\DonationServicePolicy;
use App\Policies\LocationTypePolicy;
use App\Policies\MerchantPolicy;
use App\Policies\NotificationPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\PromotionPolicy;
use App\Policies\ResponseLogPolicy;
use App\Policies\RolePolicy;
use App\Policies\UserPolicy;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        User::class => UserPolicy::class,
        Category::class => CategoryPolicy::class,
        Brand::class => BrandPolicy::class,
        Promotion::class=> PromotionPolicy::class,
        Branch::class => BranchPolicy::class,
        Role::class => RolePolicy::class,
        Permission::class=> PermissionPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();
    }
}
