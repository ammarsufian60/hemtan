<?php

namespace App\Nova;

use App\Nova\Actions\ChangeUserStatus;
use App\Nova\Lenses\AdminUsers;
use App\Nova\Lenses\AppUsers;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\DateTime;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Vyuldashev\NovaPermission\Role;
use Laravel\Nova\Fields\MorphToMany;
use Vyuldashev\NovaPermission\Permission;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Domains\User\Models\User::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';



    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name','nickname', 'email', 'wallet_number'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Text::make('Name','name')
                ->rules('required', 'max:255')->hideWhenCreating()->hideWhenUpdating(),

            Text::make('Email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->hideWhenUpdating(),

            Text::make('Mobile Number')->hideWhenCreating()->hideWhenUpdating(),

            DateTime::make('Joined At','created_at')->sortable()->hideWhenCreating()->hideWhenUpdating()->hideFromIndex(),

             Text::make('Status',function(){
              if($this->is_verified == -1 )
                return 'Rejected';
              elseif($this->is_verified ==1)
                return 'Accepted';
              else
                return 'Pinding';
            }),

            MorphToMany::make('Roles', 'roles', Role::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [

        ];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new ChangeUserStatus())
        ];
    }

}
