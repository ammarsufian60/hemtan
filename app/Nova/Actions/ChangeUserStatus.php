<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Laravel\Nova\Fields\Select;
use ReleansAPILib\APIException;
use ReleansAPILib\ReleansAPIClient;


class ChangeUserStatus extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        collect($models)->each(function($user)use ($fields){
            if($user->is_verified ==0 )
            {
                $otp = mt_rand(1000,9999);
                $user->update(['is_verified' => $fields->status , 'otp' => $otp ]);

    	        $oAuthAccessToken = 'e01b3e958fac567e3e0e30db39402fbd'; // OAuth 2.0 Access Token
	        	$client = new ReleansAPIClient($oAuthAccessToken);
		        $message = $client->getMessage();
		        $accept = 'Accept';
		        $senderId = 'R7BErm0wMvbmwjEeYAlO1nqVy';
		        $mobileNumber =(string) "+962".substr($user->mobile_number,strlen($user->mobile_number)-9); //$user->moblie_number;
		        $body = 'Your Himtna Verification code is :  '.$otp;

		        $result = $message->createSendSMSMessage($accept, $senderId, $mobileNumber, $body);
            }
        });
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            Select::make('Status')->options([
             1 =>   'Accept' ,
            -1 =>   'Rejected' ,
             0 =>   'Pending',
            ])
        ];
    }
}
