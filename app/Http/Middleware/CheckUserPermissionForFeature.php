<?php

namespace App\Http\Middleware;

use App\Domains\User\Models\User;
use Closure;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CheckUserPermissionForFeature
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $feature)
    {
        if(Auth::user()->hasPermissionTo($feature))
            return $next($request);
        else
            throw new HttpException(403, "User doesn't have permission to access this feature");
    }
}
