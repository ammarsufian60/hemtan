<?php


if(! function_exists('haversign')) {
    function haversign($srtLat, $srcLong, $desLat, $destLong)
    {
        $earth_radius = 6371;

        $dLat = deg2rad($desLat - $srtLat);
        $dLon = deg2rad($destLong - $srcLong);

        $a = sin($dLat / 2) * sin($dLat / 2) + cos(deg2rad($srtLat)) * cos(deg2rad($desLat)) * sin($dLon / 2) * sin($dLon / 2);
        $c = 2 * asin(sqrt($a));
        $d = $earth_radius * $c;

        return floatval($d);
    }
}
