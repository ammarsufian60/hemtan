<?php


namespace App\Domains\Promotion\Actions;


use App\Domains\Promotion\Models\Promotion;

class PromotionList
{

    public function __construct($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function handle()
    {
        if(is_null($this->filter)){
            $promotions =Promotion::where('status',true)->get();
        }
        else{
            $promotions =Promotion::where('status',true)->where('preferred_offer',true)->get();
        }
        return $promotions;
    }
}
