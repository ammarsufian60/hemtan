<?php


namespace App\Domains\Promotion\Http\Controllers;


use App\Domains\Promotion\Models\Promotion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GenerateQRController extends Controller
{

    public function __invoke(Request $request)
    {

        $this->validate($request, [
            'promotion_id' => ['required','exists:promotions,id'],
        ]);
        $promotion =Promotion::find($request->promotion_id);

        $params =[
             'brand' => $promotion->brand->name,
             'name' => $promotion->name,
             'price' => $promotion->price,
             'quantity' => 1,
         ];

        $qr = \QrCode::format('png')->generate('brand name : '.$promotion->brand->name.',name : '.$promotion->name.',discounted percentage: '. (int) $promotion->discounted_rate.'%'.',generated at : '.\Carbon\Carbon::now()->format('yy-m-d h:m:s'));

        return response()->json(['message' => base64_encode($qr)]);
    }
}
