<?php


namespace App\Domains\Promotion\Http\Controllers;


use App\Domains\Promotion\Actions\PromotionList;
use App\Domains\Promotion\Http\Resources\PromotionResource;
use App\Domains\Promotion\Models\Promotion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PromotionController extends Controller
{

    public function index(Request $request)
    {
        $filter=null ;
        if($request->has('navbar'))
        {
            $filter = $request->navbar;
        }

        $promotions = (new PromotionList($filter))->handle();

        return PromotionResource::collection($promotions);
    }
}
