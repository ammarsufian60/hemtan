<?php


namespace App\Domains\Promotion\Http\Resources;


use App\Domains\Brand\Http\Resources\BrandResources;
use App\Domains\Brand\Models\Brand;
use App\Domains\Category\Http\Resources\CategoryResource;
use App\Domains\Category\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;

class PromotionResource extends JsonResource
{
    /**
     *   'brand_id',
    'category_id',
     */
    public function toArray($request)
    {
      return [
          'id'=> $this->id,
          'name' =>[
              'en' =>$this->name,
              'ar' => $this->name_ar,
          ],
          'description' =>[
              'en'=> $this->description,
              'ar' => $this->description_ar,
           ],
          'discounted' => (int) $this->discounted_rate,
          'image' => $this->image,
          'logo' => $this->brand->image,

      ];
    }
}
