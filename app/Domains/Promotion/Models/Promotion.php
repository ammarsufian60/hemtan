<?php

namespace App\Domains\Promotion\Models;

use App\Domains\Brand\Models\Brand;
use App\Domains\Category\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Promotion extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable =[
        'name',
        'name_ar',
        'description',
        'description_ar',
        'discounted_rate',
        'brand_id',
        'category_id',
        'status',
        'preferred_offer',
    ];
     /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function getImageAttribute()
    {
        $image = $this->getFirstMediaUrl('promotion');
        return empty($image)? null :config('app.url') . $image;
    }

    public function getQrAttribute()
    {
        $image = $this->getFirstMediaUrl('qr');
        return empty($image)? null :config('app.url') . $image;
    }
}
