<?php


namespace App\Domains\User\Actions;


use App\Domains\User\Models\User;
use Illuminate\Support\Facades\Hash;

class LoginUser
{
    protected $params;

    /**
     * LoginUser constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        
//       dd($this->params['otp']);
        $user =User::where('mobile_number', $this->params['mobile_number'])->first();

        if($user->is_blocked){
            throw new \Exception('Your Account is blocked ');
        }
        if($this->params['otp'] !== $user->otp)
        {
            throw new \Exception('Something wrong in credential');
        }

        if($user->is_verified == 1)
        {
            return $user;

        }elseif($user->is_verified == 0)
        {
            throw new \Exception('we are checking your Account data');
        }else
        {
            throw new \Exception('Sorry, your account is rejected');
        }


    }

}
