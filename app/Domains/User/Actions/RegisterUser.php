<?php


namespace App\Domains\User\Actions;

use App\Domains\User\Models\User;
use Illuminate\Support\Facades\Hash;

class RegisterUser
{
    protected $params;

    /**
     * RegisterUser constructor.
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params =$params;
    }

    /**
     * @return mixed
     */
    public function  handle()
    {
        $user = User::create([
            'name' => $this->params['name'],
//            'password' => Hash::make($this->params['password']),
            'email' => $this->params['email'],
            'mobile_number' =>$this->params['mobile_number'],
            'is_verified' => 0,
        ]);

        if($this->params['is_merchant']){
           $user->assignRole('merchant');
        }else{
            $user->assignRole('normal');
        }

        return $user;

   }
}
