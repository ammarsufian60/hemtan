<?php


namespace App\Domains\User\Http\Resources;

use App\Domains\Promotion\Http\Resources\PromotionResource;
use App\Domains\Promotion\Models\Promotion;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;

class UserAuthResource extends  JsonResource
{

    public function toArray($request){
        return [
            'id' =>$this->id,
            'user_name' =>$this->name,
            'email' => $this->email,
            'token' => $this->createToken('user')->accessToken,
//            'favorites' => PromotionResource::collection(Promotion::whereIn('id', $this->favorites->pluck('promotion_id'))),
		'is_merchant'=> $this->roles->first()->name =='normal'?0 :1,
        ];
    }


}
