<?php


namespace App\Domains\Category\Http\Resources;


use App\Domains\Category\Models\Category;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Domains\Brand\Http\Resources\BrandResources;

class CategoryResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name'=>[
                'en' => $this->name,
                'ar' => $this->name_ar,
            ],
//            'brands' => BrandResources::collection($this->brands),
            'image' => $this->image,
      ];
    }

    protected  function getSubCategories($category_id)
    {
        $categories = Category::where('parent_category_id',$category_id)->get();

        return $categories ;
    }
}
