<?php


namespace App\Domains\Category\Http\Controller;

use App\Domains\Category\Actions\CategoryList;
use App\Domains\Category\Http\Resources\CategoryResource;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        try{
            $categories =(new CategoryList())->handle();

        }catch (\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()] ,500);
        }

        return CategoryResource::collection($categories);
    }
}
