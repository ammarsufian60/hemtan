<?php

namespace App\Domains\Category\Models;

use App\Domains\Brand\Models\Brand;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Category extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable = [
        'name',
        'name_ar',
        'parent_category_id',
        'status',
    ];

    protected $hidden =[ 'status','created_at','updated_at','parent_category_id'];
   
    public function parent()
    {
        return $this->belongsTo(self::class,  'parent_category_id');
    }

    public function brands()
    {
        return $this->hasMany(Brand::class);
    }

    public function getImageAttribute()
    {
        $image = $this->getFirstMediaUrl('category');
        return empty($image)? null :config('app.url') . $image;
    }
}
