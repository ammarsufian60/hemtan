<?php


namespace App\Domains\Category\Actions;


use App\Domains\Category\Models\Category;

class CategoryList
{

    public function __construct()
    {

    }

    public function handle()
    {
        return Category::whereNull('parent_category_id')->where('status',true)->get();
    }
}
