<?php


namespace App\Domains\Favorite\Actions;


use App\Domains\Favorite\Models\Favorite;

class StoreFavorite
{
    protected  $promotion_id ;

    /**
     * StoreFavorite constructor.
     * @param $promotion_id
     */
    public function  __construct($promotion_id)
    {
        $this->promotion_id = $promotion_id ;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $favorite = Favorite::updateOrCreate([
            'user_id' => \Auth::user()->id,
            'promotion_id' => $this->promotion_id,
        ]);

        return $favorite;
    }
}
