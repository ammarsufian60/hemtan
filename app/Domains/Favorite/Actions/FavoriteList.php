<?php


namespace App\Domains\Favorite\Actions;


use App\Domains\Favorite\Models\Favorite;
use App\Domains\Promotion\Http\Resources\PromotionResource;
use App\Domains\Promotion\Models\Promotion;

class FavoriteList
{

    public function __construct()
    {

    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function handle()
    {
        $favorites = Favorite::where('user_id' , \Auth::user()->id)->pluck('promotion_id');

        $promotions = Promotion::whereIn('id',$favorites)->get();

        return PromotionResource::collection($promotions);


    }
}
