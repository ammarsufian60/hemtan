<?php


namespace App\Domains\Favorite\Http\Controllers;


use App\Domains\Favorite\Actions\FavoriteList;
use App\Domains\Favorite\Actions\StoreFavorite;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FavoriteController extends Controller
{

    public function index(Request  $request)
    {
        $favorites = (new FavoriteList())->handle();

        return $favorites;
    }

    public function store(Request  $request)
    {
         $request->validate([
            'promotion_id' => 'required',
        ]);
         try{
              $favorite = (new StoreFavorite($request->promotion_id ))->handle();
         }catch (\Exception $e)
         {
              return response()->json(['message' => $e->getMessage()] ,500);
         }


       return response()->json(['message' => 'success']);

    }
}
