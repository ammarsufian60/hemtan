<?php

namespace App\Domains\Favorite\Models;

use App\Domains\Promotion\Models\Promotion;
use App\Domains\User\Models\User;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $fillable=[
        'promotion_id',
        'user_id',
    ];

    public $timestamps = false;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function promotion()
    {
        return $this->hasMany(Promotion::class);
    }

}
