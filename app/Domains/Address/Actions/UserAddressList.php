<?php


namespace App\Domains\Address\Actions;


use App\Domains\Address\Models\Address;

class UserAddressList
{
    protected $user_id;

    /**
     * @param $user_id
     */
    public function __construct($user_id)
    {
     $this->user_id=$user_id;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $addresses = Address::where('user_id',$this->user_id)->get();

        return $addresses;
    }
}
