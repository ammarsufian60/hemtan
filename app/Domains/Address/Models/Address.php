<?php

namespace App\Domains\Address\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable =[
        'city',
        'area',
        'latitude',
        'longitude',
        'is_default',
        'type',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
