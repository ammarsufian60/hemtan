<?php


namespace App\Domains\Address\Http\Controllers;


use App\Domains\Address\Http\Requests\StoreAddressesRequest;
use App\Domains\Address\Http\Resources\AddressResources;
use App\Facades\Address;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddressController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $addresses = Address::index(Auth::user()->id);

        return AddressResources::collection($addresses);
    }

    /**
     * @param StoreAddressesRequest $request
     * @return AddressResources
     */
    public function store(StoreAddressesRequest $request)
    {
        $address = Address::store($request->all());

        return new AddressResources($address);
    }
}
