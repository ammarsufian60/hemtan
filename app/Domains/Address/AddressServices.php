<?php


namespace App\Domains\Address;


use App\Domains\Address\Actions\StoreUserAddress;
use App\Domains\Address\Actions\UserAddressList;

class AddressServices
{
    /**
     * @param int $user_id
     * @return mixed
     */
    public function index(int $user_id)
    {
        return (new UserAddressList($user_id))->handle();
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function store(array $params)
    {
        return (new StoreUserAddress($params))->handle();
    }
}
