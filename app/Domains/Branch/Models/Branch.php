<?php

namespace App\Domains\Branch\Models;

use App\Domains\Brand\Models\Brand;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable= [
        'name',
        'latitude',
        'longitude',
        'brand_id',
        'status',
        'is_main',
        'location',
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class );
    }
}
