<?php


namespace App\Domains\Branch\Http\Resources;


use App\Domains\Brand\Http\Resources\BrandResources;
use Illuminate\Http\Resources\Json\JsonResource;

class BranchResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
       return [
           'name' =>$this->name,
           'latitude' => $this->latitude,
           'longitude' => $this->longitude,
           'open_hour' => $this->brand->open_hour,
           'close_hour' => $this->brand->close_hour,
           'location' => $this->location,
//           'brand' => new BrandResources($this->brand),
       ];
    }
}
