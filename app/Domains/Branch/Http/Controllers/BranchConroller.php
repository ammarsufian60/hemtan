<?php


namespace App\Domains\Branch\Http\Controllers;


use App\Domains\Branch\Actions\NearerBranches;
use App\Domains\Branch\Http\Resources\BranchResources;
use App\Facades\BranchFacade;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BranchConroller extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function nearby(Request $request)
    {
       $this->validate($request, [
        'latitude' =>['required' ,'string'],
        'longitude' => ['required','string'],
      ]);

       $branches = (new NearerBranches($request->latitude , $request->longitude))->handle();

      return BranchResources::collection($branches);
    }
}
