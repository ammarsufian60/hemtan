<?php


namespace App\Domains\Branch\Actions;


use App\Domains\Branch\Models\Branch;

class NearerBranches
{
    protected $latitude;
    protected $longitude;

    /**
     * NearerBranches constructor.
     * @param $latitude
     * @param $longitude
     */
    public function __construct($latitude , $longitude)
    {
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        $locations = Branch::get();

        $locations = $locations->reject(function (Branch $branch) {
            return haversign($this->latitude,$this->longitude,$branch->latitude,$branch->longitude) > 2;
        });


       return $locations ;
    }
}
