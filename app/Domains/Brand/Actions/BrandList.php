<?php


namespace App\Domains\Brand\Actions;


use App\Domains\Brand\Models\Brand;
use Auth;

class BrandList
{

    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * @return mixed
     */
    public function handle()
    {
        if($this->user->is_merchant == "normal")
          return Brand::where('status',true)->get();
        else
          return Brand::where('user_id', $this->user->id)->where('status',true)->get();
    }
}
