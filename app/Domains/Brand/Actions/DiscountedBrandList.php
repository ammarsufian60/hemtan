<?php


namespace App\Domains\Brand\Actions;


use App\Domains\Brand\Models\Brand;

class DiscountedBrandList
{
  public function __construct()
  {

  }

  public function handle()
  {
     return  Brand::where('status',1)->where('is_discount', 1)->get();
  }
}
