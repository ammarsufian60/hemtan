<?php

namespace App\Domains\Brand\Models;

use App\Domains\Branch\Models\Branch;
use App\Domains\Category\Models\Category;
use App\Domains\Promotion\Models\Promotion;
use App\Domains\User\Models\User;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Brand extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $fillable =[
        'name',
        'name_ar',
        'user_id',
        'status',
        'open_hour',
        'close_hour',
        'category_id',
        'is_discounted',
        'services',
        'services_ar',
	'description',
	'description_ar',
     ];

    public function owner()
    {
        return $this->belongsTo(User::class , 'user_id');
    }

    public function category()
    {
       return $this->belongsTo(Category::class);
    }
    public function branches()
    {
        return $this->hasMany(Branch::class);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }

    public function getImageAttribute()
    {
        $image = $this->getFirstMediaUrl('brand');
        return empty($image)? null :config('app.url') . $image;
    }
    public function getLogoAttribute()
    {
        $image = $this->getFirstMediaUrl('brand');
        return empty($image)? null :config('app.url') . $image;
    }
}
