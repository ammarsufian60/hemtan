<?php


namespace App\Domains\Brand\Http\Controllers;


use App\Domains\Brand\Actions\DiscountedBrandList;
use App\Domains\Brand\Http\Resources\BrandResources;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DiscountedBrandsController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function __invoke(Request $request)
    {
        $brands =(new DiscountedBrandList())->handle();
        return BrandResources::collection($brands);
    }
}
