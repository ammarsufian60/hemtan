<?php


namespace App\Domains\Brand\Http\Controllers;


use App\Domains\Brand\Actions\BrandList;
use App\Domains\Brand\Http\Resources\BrandResources;
use App\Http\Controllers\Controller;

class BrandController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        try{
            $brands = (new BrandList())->handle();

        }catch (\Exception $e)
        {
            return response()->json(['message' => $e->getMessage()] ,500);
        }

        return BrandResources::collection($brands);
    }

}
