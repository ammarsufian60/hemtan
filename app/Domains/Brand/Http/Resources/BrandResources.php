<?php


namespace App\Domains\Brand\Http\Resources;


use App\Domains\Branch\Http\Resources\BranchResources;
use Illuminate\Http\Resources\Json\JsonResource;

class BrandResources extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
       return [
      	'id' => $this->id,
        'name' => [
           'en' => $this->name,
           'ar' => $this->name_ar,
           ],
         'services' =>[
  	   'en' => $this->services,
            'ar' => $this->services_ar
         ],
         'description' =>[
           'en' => $this->description,
            'ar' => $this->description_ar
         ],
	    'logo' => $this->logo,
        'image' => $this->image,
        'main_location' => new BranchResources($this->branches->where('is_main',true)->first()),
        'discount' => (int) $this->promotions->pluck('discounted_rate')->max(),
	'open_time' => $this->open_hour,
        'close_time' => $this->close_hour,

       ];
    }
}
